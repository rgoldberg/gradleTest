= GradleTest Gradle Plugin
:toc:
:readme-base-url: https://gitlab.com/ysb33rOrg/gradleTest/blob/
:docs-base-url: http://ysb33rOrg.gitlab.io/gradleTest/

Test your plugin against different versions of Gradle as part of your build. This plugin extends the power that the new
`GradleTestKit` brings without having to actually author code. It rather allows plugins authors to write functional tests
that looks like normal Gradle projects (multi-project is supported). This also makes it easier to create sample projects
that can directly be used in documentation.

NOTE: *If you are using a Gradle version < 2.13, it will run in legacy mode and behave like the 0.5.5 version of the plugin*.

NOTE: If you used some of the 1.0-betaXYZ versions you might have encountered GradleRunner being shipped. It has been extracted before the final 1.0 release and now lives in its own https://gitlab.com/ysb33rOrg/gradle-runner-plugin[repository on GitLab]

== Previous versions of this document

* {readme-base-url}RELEASE_1_0/README.adoc[1.0]
* {readme-base-url}RELEASE_0_5_5/README.adoc[0.5.5]
* {readme-base-url}RELEASE_0_5_2/README.adoc[0.5.2]
* {readme-base-url}RELEASE_0_5_1/README.adoc[0.5.1]
* {readme-base-url}RELEASE_0_5/README.adoc[0.5]

== Documentation

Please visit the {docs-base-url}[documentation website] for this plugin

== Contributions

* https://github.com/dcendents[Daniel Beland] - Gradle 2.5 fixes.
* https://github.com/szpak[Marcin Zajączkowski] - Fix for Zip errors.
* https://github.com/matthiasbalke[Matthias Balke] - Documentation.
* https://github.com/scphantm[Willie Slepecki] - Multiple build files in one test project folder.
* https://github.com/scphantm[Willie Slepecki] - Load distributions from enterprise repositories.

If you would like to contribute fixes, please see `HACKING.adoc`.

For a list of kudos please see the {docs-base-url}[website]

