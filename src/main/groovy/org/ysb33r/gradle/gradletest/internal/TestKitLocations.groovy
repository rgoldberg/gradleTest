package org.ysb33r.gradle.gradletest.internal

/** Strategy for managing the TestKit directory location.
 *
 * @since 2.0
 */
enum TestKitLocations {

    /** Destroy the Testkit directory after the test is completed.
     *
     * Tests run faster, but problems can be harded to diagnose.
     */
    TEMPORARY,

    /** Keep the Testkit data, but share the directory between the complete testset.
     *
     */
    PER_GROUP,

    /** Keep one TestKit directory per test.
     *
     * This options uses the most space, but allows detailed analysis per test.
     */
    PER_TEST
}