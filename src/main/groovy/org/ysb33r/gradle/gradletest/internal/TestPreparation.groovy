package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import org.ysb33r.gradle.gradletest.TestDefinition

/** Describes some basic attributes for preparing a test
 *
 * @since 2.0
 */
@CompileStatic
class TestPreparation {
    File targetDir
    String testBase
    String defaultTask
    File manifestFile
    File workDir
    TestDefinition testDefinitions
    boolean willFail
    boolean deprecationMessageMode
    boolean withDebug
    boolean cleanCache
    TestKitLocations testKitLocations
}
