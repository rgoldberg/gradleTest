package org.ysb33r.gradle.gradletest.internal

import groovy.transform.CompileStatic
import org.gradle.testing.jacoco.plugins.JacocoPluginExtension
import org.gradle.testing.jacoco.tasks.JacocoReportBase
import org.ysb33r.gradle.gradletest.GradleTest
import org.ysb33r.gradle.gradletest.TestGenerator

/** Configures GradleTest tasks for Jacoco.
 *
 * @since 2.0
 */
@CompileStatic
class Jacoco {

    /** Configures a GradleTest task to work with Jacoco.
     *
     * @param task GradleTest task to configure.
     */
    static void configure(final GradleTest task) {
        task.debugTests = true
        JacocoReportBase reporter = (JacocoReportBase)(task.project.tasks.getByName('jacocoTestReport'))
        reporter.executionData(task)
    }

    /** Conditionally configures a GradleTest task depending on the knowledge that the
     * associated TestGenerator task has about it.
     *
     * @param task TestGenerator task to query
     */
    static void configure(final TestGenerator task) {
        if(!task.testMap.isEmpty()) {
            configure((GradleTest)(task.project.tasks.getByName(task.linkedTestTaskName)))
        }
    }
}
